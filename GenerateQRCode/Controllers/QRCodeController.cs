﻿using Microsoft.AspNetCore.Mvc;

namespace GenerateQRCode.Controllers
{
    [Route("qrcode")]
    public class QRCodeController : Controller
    {
        [Route("index")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("generate")]
        public IActionResult Generate(string productId)
        {
            ViewBag.productId = productId;
            return View("Index");
        }
    }
}